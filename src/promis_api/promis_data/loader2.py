import os
import sys
import logging

sys.path.append("/home/elena/workspace/promis/src/promis_api/")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "promis_api.settings")

from parsers import variant_parser2, potential_parser2
#from django.core import serializers
from django.db.utils import IntegrityError
from datetime import *
#import time

logging.basicConfig(level=logging.INFO,  # DEBUG massages will be omitted
                    filename=str(str(datetime.now()) + '.log'),
                    filemode='w',
                    format='%(asctime)s %(lineno)s %(levelname)-8s %(message)s')

def loader(parser, path):

    try:
        parser.parser(path)
    except IntegrityError:
        logging.debug("already exists")  # now omitted
    except Exception, e:
        raise Exception(e)

path_variant = '/home/len/Variant/Data_Release1'
#path = '/home/elena/workspace/Data_Release1_2_session/597/'
dirs_variant = []
for folder_name in next(os.walk(path_variant))[1]:
    dirs_variant.append(os.path.join(path_variant, folder_name))
#print dirs

for path in dirs_variant:
    logging.info('%s directory of Variant project is loading' % path)
    loader(variant_parser2, path)
    logging.info('%s directory of Variant project has been loaded' % path)

path_potential = '/home/len/Potential/DECODED'
dirs_potential = []
for folder in next(os.walk(path_potential))[1]:
    subdir = str(folder + '/pdata' + folder)
    dirs_potential.append(os.path.join(path_potential, subdir))
#print dirs_potential

for path in dirs_potential:
    logging.info('%s directory of Potential project is loading' % path)
    loader(potential_parser2, path)
    logging.info('%s directory of Potential project has been loaded' % path)